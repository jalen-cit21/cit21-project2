FROM php:8.2-fpm-alpine3.18

RUN apk update
RUN apk add nginx

RUN docker-php-ext-install mysqli

COPY . /var/www/food_paradise

COPY ./php_crud.conf /etc/nginx/http.d/php_crud.conf

EXPOSE  9090

CMD php-fpm -D; nginx -g "daemon off;"